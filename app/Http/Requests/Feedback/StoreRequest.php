<?php

namespace App\Http\Requests\Feedback;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'phone' => 'required',
            'email' => 'required|string|email',
            'message' => 'nullable',
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Имя обязательное поле',
            'name.string' => 'Введите корректное имя',
            'phone.required' => 'Номер телефона обязательное поле',
            'email.required' => 'Эл. почта обязательное поле',
            'email.email' => 'Введите корректную эл. почту',
            'email.string' => 'Введите корректную эл. почту',
        ];
    }
}
