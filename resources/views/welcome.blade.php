@extends('layouts.main')

@section('welcome')
    <h1 class="mb-5">Тестовое задание back-end</h1>
    <div class="d-flex flex-column gap-2">
        <a href="feedback/">Все заявки</a>
        <a href="feedback/create">Создать заявку</a>
    </div>
@endsection
