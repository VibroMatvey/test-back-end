<?php

namespace App\Http\Controllers\Feedback;

use App\Http\Controllers\Controller;
use App\Http\Resources\Feedback\FeedbackResource;
use App\Models\Feedback;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        $feedback_items = FeedbackResource::collection(Feedback::all());
        return view('feedback.index', compact('feedback_items'));
    }
}
