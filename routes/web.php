<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'feedback'], function () {
    Route::get('/', \App\Http\Controllers\Feedback\IndexController::class)->name('feedback.index');
    Route::get('/create', \App\Http\Controllers\Feedback\CreateController::class)->name('feedback.create');
    Route::post('/', \App\Http\Controllers\Feedback\StoreController::class)->name('feedback.store');
});

Auth::routes();

