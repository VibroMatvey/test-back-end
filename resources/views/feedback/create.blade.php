@extends('layouts.main')

@section('form')
    <div>
        <div class="d-flex flex-row justify-content-between">
            <h1>Создать заявку</h1>
            <p>
                <a href="/feedback/" class="text-primary">Все заявки</a>
            </p>
        </div>
        <form action="{{route('feedback.store')}}" class="mt-5" method="POST">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label d-flex justify-content-between">
                    <span>Имя</span>
                    @error('name')
                    <span class="badge rounded-pill bg-danger">
                        {{ $message }}
                    </span>
                    @enderror
                </label>
                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="John">
            </div>
            <div class="mb-3">
                <label for="phone" class="form-label d-flex justify-content-between">
                    <span>Номер телефона</span>
                    @error('phone')
                    <span class="badge rounded-pill bg-danger">
                        {{ $message }}
                    </span>
                    @enderror
                </label>
                <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone" placeholder="89999999999">
            </div>
            <div class="mb-3">
                <label for="email" class="form-label d-flex justify-content-between">
                    <span>Эл почта</span>
                    @error('email')
                    <span class="badge rounded-pill bg-danger">
                        {{ $message }}
                    </span>
                    @enderror
                </label>
                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="name@example.com">
            </div>
            <div class="mb-3">
                <label for="message" class="form-label">Сообщение</label>
                <textarea class="form-control" name="message" id="message" rows="3"></textarea>
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-primary mb-3">Отправить</button>
            </div>
        </form>
    </div>
@endsection
