<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite(['resources/js/app.js'])
    <title>Тестовое задание Back-end</title>
</head>
<body>
    <main class="container mt-5 mx-auto">
        @yield('list')
        @yield('form')
        @yield('welcome')
    </main>
</body>
</html>
