@extends('layouts.main')

@section('list')
    <div>
        <div class="d-flex flex-row justify-content-between">
            <h1>Заявки</h1>
            <p>
                <a href="/feedback/create" class="text-success">Создать заявку</a>
            </p>
        </div>
        <table class="table mt-5">
            <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Message</th>
            </tr>
            </thead>
            <tbody>
            @foreach($feedback_items as $item)
                <tr>
                    <th>{{ $item->id }}</th>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->phone }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->message }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
